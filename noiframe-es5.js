"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _readOnlyError(name) { throw new Error("\"" + name + "\" is read-only"); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var NoIframe =
/*#__PURE__*/
function () {
  function NoIframe(elementId, url, assets) {
    _classCallCheck(this, NoIframe);

    this._assets = assets;
    this._urlToLoad = url;
    this._elementId = this._cleanID(elementId);
    Object.freeze(this);
  }

  _createClass(NoIframe, [{
    key: "load",
    value: function load() {
      this._addCss();

      return this._loadHtml();
    }
  }, {
    key: "_cleanID",
    value: function _cleanID(elementId) {
      // Replace `#` char in case the function gets called `querySelector` or jQuery style
      var elemId = elementId;

      if (elementId.startsWith("#")) {
        elemId = (_readOnlyError("elemId"), elementId.replace("#", ""));
      }

      return elemId;
    }
  }, {
    key: "_addCss",
    value: function _addCss() {
      for (var i = 0; i < this._assets.css.length; i++) {
        var d = document,
            h = d.getElementsByTagName('head')[0],
            l = d.createElement('link');
        l.rel = 'stylesheet';
        l.type = 'text/css';
        l.async = true;
        l.href = this._assets.css[i];
        h.appendChild(l);
      }
    }
  }, {
    key: "_addJs",
    value: function _addJs() {
      var promises = this._assets.js.map(function (js) {
        return new Promise(function (resolve, reject) {
          var d2 = document,
              h2 = d2.getElementsByTagName('head')[0],
              s2 = d2.createElement('script');
          s2.type = 'text/javascript';
          s2.async = true;
          s2.onload = resolve;
          s2.onerror = reject;
          s2.src = js;
          h2.appendChild(s2);
        });
      });

      return Promise.all(promises);
    }
  }, {
    key: "_loadHtml",
    value: function _loadHtml() {
      var _this = this;

      return this._get(this._urlToLoad).then(function (body) {
        return document.getElementById(_this._elementId).innerHTML = body;
      }).then(function () {
        return _this._addJs();
      });
    }
  }, {
    key: "_get",
    value: function _get(url) {
      var _this2 = this;

      var init = {
        method: "GET",
        headers: {
          "Content-Type": "text/html"
        },
        mode: "cors",
        cache: "default"
      };
      return fetch(new Request(url, init)).then(function (res) {
        return _this2._handleErrors(res);
      }).then(function (res) {
        return res.text();
      });
    }
  }, {
    key: "_handleErrors",
    value: function _handleErrors(res) {
      if (!res.ok) throw new Error(res.statusText);
      return res;
    }
  }]);

  return NoIframe;
}();
