class NoIframe {

    constructor(elementId, url, assets) {
        this._assets = assets;
        this._urlToLoad = url;
        this._elementId = this._cleanID(elementId);
        Object.freeze(this);
    }

    load() {
        this._addCss();
        return this._loadHtml();
    }
    _cleanID(elementId) {
         // Replace `#` char in case the function gets called `querySelector` or jQuery style
        const elemId = elementId;
        if (elementId.startsWith("#")) {
            elemId = elementId.replace("#", "");
        }
        return elemId;
    }
    _addCss() {
        for(let i = 0; i < this._assets.css.length; i++){
            let d=document,
            h=d.getElementsByTagName('head')[0],
            l=d.createElement('link');
            l.rel='stylesheet';
            l.type='text/css';
            l.async=true;
            l.href=this._assets.css[i];
            h.appendChild(l);
        }
    }
    _addJs() {
        let promises = this._assets.js.map(js => {
            return new Promise((resolve, reject) => {
                let d2=document,
                h2=d2.getElementsByTagName('head')[0],
                s2=d2.createElement('script');
                s2.type='text/javascript';
                s2.async=true;
                s2.onload = resolve;
                s2.onerror = reject;
                s2.src=js;
                h2.appendChild(s2);
            })
        })

        return Promise.all(promises);
    }
    _loadHtml() {
        return this._get(this._urlToLoad)
        .then(body => document.getElementById(this._elementId).innerHTML = body)
        .then(() => this._addJs());
    }
    _get(url) {
        const init = {
            method : "GET",
            headers : { "Content-Type" : "text/html" },
            mode : "cors",
            cache : "default"
        };
        return fetch(new Request(url, init))
            .then(res => this._handleErrors(res))
            .then(res => res.text());
    }
    _handleErrors(res) {
        if(!res.ok) throw new Error(res.statusText);
        return res;
    }
}
